FROM openjdk:10-jre-slim
COPY ./target/MySpringBootDocker-0.0.1-SNAPSHOT.jar /usr/src/hola/
WORKDIR /usr/src/hola
EXPOSE 8080
ENTRYPOINT ["java","-jar", "MySpringBootDocker-0.0.1-SNAPSHOT.jar"]