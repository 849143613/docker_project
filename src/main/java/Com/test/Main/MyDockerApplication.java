package Com.test.Main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MyDockerApplication {

        @RequestMapping("/rest/")
        public String home() {
				System.out.println("Hello Docker");
                return "Hello you in docker23";
        }


        public static void main(String[] args) {
                SpringApplication.run(MyDockerApplication.class, args);
        }

}
